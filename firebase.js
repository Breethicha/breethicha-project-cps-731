<script type="module">
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-analytics.js";
  import { getfirestore } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-firestore.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyCsHqb4DHMR9nKYNDGl3PNEh-kq0il3tHQ",
    authDomain: "project-ee7fc.firebaseapp.com",
    projectId: "project-ee7fc",
    storageBucket: "project-ee7fc.appspot.com",
    messagingSenderId: "43511499037",
    appId: "1:43511499037:web:1035f08a423c8c2b6a79f4",
    measurementId: "G-ZSEV9D7HRC"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);
</script>